# Ansible .NET Web Deployment

This project aims to be a starting point / proof of concept for using
ansible to provision IIS servers and deploy .NET web applications to them.

It contains playbooks, plus example inventory and configuration files which
can be used to deploy one or more web applications from the local filesystem
onto one or more windows server instances.

By running a single `ansible-playbook` command one can:
- Install IIS webserver and ASP component on the target windows machine
- Install ASP components and updates the firewall to allow API traffic
- Upload and unpack the appropriate version zip file
- Configure IIS to serve the application

The list of applications to be deployed, and the variables
that configure them (including version numbers) are selected
via a YML file.

The structure of the playbooks also allows for deployment to multiple
environments (e.g. staging, production) in a secure manner - illustrating
the use of _ansible vault_ to store sensitive configuration data for
each environment under distinct credentials.

## Contents
There are three subdirectories:

| *Directory*              |*Contents*                                           |
| -------------------------|-----------------------------------------------------|
| *POC*                    | The ansible playbooks and configuration files       |
| *toy-webapi*             | Source code for the trivial example web application |
| *experimental-playbooks* | Code snippets and notes (now obselete)              |

## Setup and Usage
The subsections below explain how to:
- Create the Ansible Control Node
- Create and prepare a Windows Server instance
- Configure the file "inventory/example" with its IP address
- Configure, and then encrypt "group_vars/example-api-servers/vault"

And then:
- Run the ansible playbook.

### Create the Ansible Control Node.
The ansible control node is a server on which the ansible
playbooks and configuration reside, and where ansible runs.

Running Ansible control node on windows is not supported:
It must be a linux machine. All mainstream linux variants are
supported - but for POC we are using a free tier AWS instance
running Ubuntu 16.04:
- Go to EC2 management console
- Choose "Ubuntu Server 16.04 LTS (HVM), SSD Volume Type" as the image type
- Accept the default size (t2.micro) and launch the instance.
- Wait for it to boot
- SSH in
  ```
  ssh -i ~/.ssh/aws.pem ubuntu@xx.xx.xx.xx
  ```
- Become root:
  ```
  sudo -s -H
  ```

An older version (2.1.1.0) of ansible is available from the standard
Ubuntu repository - however it is recommended to use the more recent
version available from the Ansible PPA, as this has some recently
added modules which are useful for managing windows
machines (such as win_shell). To add this PPA:
```
# add-apt-repository ppa:ansible/ansible
# sudo apt-get update
```

and then
```
# apt-get install ansible
```

Some additional python libraries are also needed, to allow ansible to
connect to windows machines (since windows does not natively support
SSH). To install these libraries:
```
# apt-get install python-dev libkrb5-dev
# apt-get install python-pip python-setuptools
# pip install pywinrm[kerberos]
```

### Create/Prepare the Windows Server instance
For this example we are using a Windows 2016 server running on AWS EC2.
However, older Windows Server versions and other hosting environments
should also work just fine.

The steps are:
- Launch the EC2 instance
- Configure EC2 firewall
- Enable windows remote access over HTTPS

#### Launch the EC2 instance
Go to the EC2 console and launch a new instance as follows:
- Select Microsoft Windows Server 2016 Base with Containers
- Select type (for example purposes, t2.micro works fine)
- Customize settings if desired (free tier defaults seem to work)
- Select key
- Launch
- Make a note of the Ipv4 Public IP address (mine is 52.56.233.242)

#### Configure EC2 firewall
In order to perform commands on windows machines,
Ansible uses Windows Remote (WinRM) over an HTTPS
connection. By default the EC2 firewall will block this.
It will also block traffic to the web API itself. To fix this:
- Go to EC2 console
- Find your instance
- Scroll to the end, click on the security group link
- Click on "inbound" tab, then "edit"
- On "Edit inbound rules" dialog
    - For remote management:
        - Click "Add Rule", TCP, port 5986, from anywhere
    - For API access
        - Click "Add Rule", TCP, port 8080, from anywhere
- Click Save

#### Enable Windows Remote Access over HTTPS
On the Windows machine, we need to:
- Enable the WinRM over HTTPS service
- Allow it through the windows server firewall

The Ansible website has a powershell script which does these two things. To use it:

- Remote Desktop into the windows machine
- Use IE to download https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1
- Save it to your desktop
- Run it:
  ```
  .\ConfigureRemotingForAnsible.ps1
  ```

### Configure the inventory file
Back on the ansible control node, `exit` back to non-root user (elevated
permissions are not needed for running ansible), fetch the code:
```
$ git clone https://bitbucket.org/markwarren/ansible-dotnet-deployment.git
$ cd ansible-dotnet-deployment/POC
```

and check that you have at least version 2.2 of ansible:
```
$ ansible --version
ansible 2.2.2.0
  config file = /etc/ansible/ansible.cfg
  configured module search path = Default w/o overrides
```

The first thing to do is set up the inventory file.

Each environment (production, staging, ...) will have
a single inventory file, specifying the IP
addresses of each "kind of" machine making up that
environment.

In this proof of concept there is only one kind of machine - an API server.

And just one environment, "example", so there's just one inventory file, `inventory/example`.

In its most basic form the inventory file might have looked
like this:

```
[api-servers]
52.56.233.242
```
But in fact, is defined as follows:

```
# API Server Machines
[example-api-servers]
52.56.233.242

[api-servers:children]
example-api-servers
```

This extra level of indirection makes it possible to encrypt
secrets relating to the "example" environment with a different
ansible vault password from other environments (staging, ...).
But more on this below.

For the moment, just edit `inventory/example` and change the
IP address.

### Put the windows password in the "vault" file
As mentioned, ansible connects to the windows machines
using `WinRM`. The credentials it needs to do this are in
`group_vars/example-api-servers/vars`:

```
ansible_user: Administrator
ansible_password: "{{vault.winrm.password}}"
ansible_port: 5986
...
```

Sensitive data items like `ansible_password` are not explicitly
defined here, but instead reference variables defined in a separate
file,  `vault`, which (in its unencrypted form), looks like this:

```
---
vault:
  winrm:
    password: xxx
  api_server:
    my_secure_var: "from ansible vault: EXAMPLE SECRET"
```

You could just enter the password for your windows box in
place of "xxx" and save the file, and run the playbook.

But clearly we want to keep credentials safe, whilst enjoying
the advantages of keeping everything in a GIT repository.

So, to encrypt the vault file enter this command, and when
prompted, enter a password of your choice:

```
$ ansible-vault encrypt group_vars/example-api-servers/vault
Vault password:
******
Encryption successful
```

The file contents are now encrypted, and look something like this:

```
$ANSIBLE_VAULT;1.1;AES256
37613839663463323531623762633439353238376237336561333839623264313230616633643365
6164306633393337376235613938383962333338373532320a303666393936663936396561343739
...
```

From now on, to edit or view the file you will need to use the `ansible-vault` command, and supply that same password:
```
$ ansible-vault edit group_vars/example-api-servers/vault
Vault password:
```

Use this command now, and change the password to the correct one
for your windows box.

### Run the Ansible Playbook

There is single top-level playbook `site.yml` that will install IIS and the
web application on the windows machine. To run it, use the `ansible-playbook`
command:

```
$ ansible-playbook -i inventory/example site.yml
```

You _should_ find this fails with the error

> ERROR! Decryption failed on .../group_vars/example-api-servers/vault

To run the playbook you need to say `--ask-vault-pass` on the 
command line, and then, when aked, supply the password you used to
encrypt the `vault` file:

```
$ ansible-playbook -i inventory/example site.yml --ask-vault-pass
```

Now, it should work. If all goes well you should get output
similar to this:

```
Vault password: 

PLAY [api-servers] *************************************************************

TASK [setup] *******************************************************************
ok: [52.56.233.242]

TASK [iis_server : install iis] ************************************************
changed: [52.56.233.242]

TASK [iis_server : install net framework] **************************************
changed: [52.56.233.242]

TASK [iis_server : install net ext] ********************************************
ok: [52.56.233.242]

TASK [iis_server : install asp net] ********************************************
ok: [52.56.233.242]

TASK [api_server : include] ****************************************************
included: /home/mark/ansible/ansible-dotnet-deployment/POC/roles/api_server/tasks/deploy_api.yml for 52.56.233.242

TASK [api_server : include] ****************************************************
included: /home/mark/ansible/ansible-dotnet-deployment/POC/roles/api_server/tasks/copy_files.yml for 52.56.233.242

TASK [api_server : config for WebApplication1] *********************************
ok: [52.56.233.242] => {
    "msg": {
        "config": {
            "my_custom_var": "from main.yml: CUSTOM VAR FOR APP1", 
            "my_secure_var": "from ansible vault: EXAMPLE SECRET", 
            "version": "0.0.2"
        }
    }
}

TASK [api_server : create directory to receive zip files] **********************
changed: [52.56.233.242]

TASK [api_server : copy zipped web application] ********************************
changed: [52.56.233.242]

TASK [api_server : unzip web application] **************************************
 [WARNING]: Module invocation had junk after the JSON data:

changed: [52.56.233.242]

TASK [api_server : set access on application files] ****************************
changed: [52.56.233.242]

TASK [api_server : include] ****************************************************
included: /home/mark/ansible/ansible-dotnet-deployment/POC/roles/api_server/tasks/create_iis_web_app_pool.yml for 52.56.233.242

TASK [api_server : create iis web application pool] ****************************
 [WARNING]: Module invocation had junk after the JSON data:

changed: [52.56.233.242]

TASK [api_server : include] ****************************************************
included: /home/mark/ansible/ansible-dotnet-deployment/POC/roles/api_server/tasks/create_iis_web_site.yml for 52.56.233.242

TASK [api_server : create empty wwwroot directory] *****************************
changed: [52.56.233.242]

TASK [api_server : set access on wwwroot directory] ****************************
changed: [52.56.233.242]

TASK [api_server : create iis web site] ****************************************
changed: [52.56.233.242]

TASK [api_server : include] ****************************************************
included: /home/mark/ansible/ansible-dotnet-deployment/POC/roles/api_server/tasks/create_iis_web_application.yml for 52.56.233.242

TASK [api_server : create iis web application] *********************************
changed: [52.56.233.242]

TASK [api_server : open firewall for http] *************************************
changed: [52.56.233.242]

PLAY RECAP *********************************************************************
52.56.233.242              : ok=21   changed=12   unreachable=0    failed=0   
```

### Checking it works

Navigate to:

http://52.56.233.242:8080/WebApplication1

replacing the IP address as appropriate, and you should get an example web page.

## Adding an environment

To add a new environment, say "staging", you will need to:
- Add a new file `inventory/staging`, based on inventory/example with obvious changes
- Add a "group_vars/staging-api-servers" directory, containing:
  - A `vars` file, which will probably be identical to the "examples" one
  - A `vault` file

To create the vault file you can:

```
$ touch group_vars/staging-api-servers/vault
$ ansible-vault encrypt group_vars/staging-api-servers/vault
```

Crucially, the vault password for the `staging` environment may be different
to the one you used for the `example` environment.

Now, edit the empty vault file:
```
$ ansible-vault edit group_vars/staging-api-servers/vault
```

and paste content similar to what you had in the example
(most likely, your windows password will also be different).

```
$ ansible-vault view group_vars/example-api-servers/vault 
```

Now you can deploy to the staging environment:

```
$ ansible-playbook -i inventory/staging site.yml --ask-vault-pass
```

and deployment to the example environment should still work (with its own vault password):

```
$ ansible-playbook -i inventory/example site.yml --ask-vault-pass
```

## Configuring The Web Application

All application config including versions is specified
in  `./roles/api_server/vars/main.yml` which looks like this:

```yaml
# Web applications to deploy.
deploy_api_list:
  - WebApplication1
#  - WebApplication2

# Application config
deploy_api_config:
  WebApplication1:
    version: 0.0.2
    my_secure_var: "{{vault.api_server.my_secure_var}}"
    my_custom_var: "from main.yml: CUSTOM VAR FOR APP1"
  WebApplication2:
    version: 0.0.2
    my_webapp2_var: "from main.yml: CUSTOM VAR FOR APP2"
```

So to upgrade/downgrade the version it's just a case of
editing this file, and re-running the playbook.

## Caveats / Limitations
Some areas we have not considered:

### Load balancing
In practice, the production environment will have multiple application
servers, with an load balancer. It would make sense to provision and
configure the load balancer using ansible as well.

### Artifactory integration
For the purposes of this example, the zipped web application
is stored locally, as files:
- `roles/api_server/files/WebApplication1_0.0.1.zip`
- `roles/api_server/files/WebApplication1_0.0.2.zip`

In reality these zipfiles would be stored in Artifactory
and playbooks would need modifying to fetch them from there.

### Population of custom fields in the web configuration
Need to take values from the YML file (some of which may be
encrypted with ansible-vault) and use them to fill in placeholder
values in the XML file.

Currently this is not done, but the following approach ought to work:
- Generate an XML file from YML using ansible templates
  - This _only_ contains the config key/values (not the whole Config.xml) 
- On windows side, use powershell script to update Config.xml
  - Adjust appSettings section, inserting each k/v pair

### Per-environment config of application variables
Currently, application versions and configuration is defined in
`./roles/api_server/vars/main.yml`.

But at least some of these definitions should probably be moved into the
`group_vars` subdirectories, since different deployment environments will
typically want different values for them.

Or, rather than moving all the definitions, it might make sense
to do a dictionary merge of values from these
two places, so that a "default" set of values can be selectively
overridden. This might reduce duplication, but at some cost
to ease of debugging.