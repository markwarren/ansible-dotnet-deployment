# Notes on IIS provisioning

## Configuring via PowerShell
See [article on technet.microsoft.com](https://technet.microsoft.com/en-us/library/hh867843(v=wps.630).aspx)

### Create wwwroot directory
An IIS "site" must have a physical location corresponding to the root (like "DocumentRoot" in Apache2).
So need to make an empty directory to serve this purpose, and make it readable/executabe
by "Users" so that IIS can access it.

```powershell
mkdir c:\Users\Administrator\Desktop\toy-app\wwwroot
icacls c:\Users\Administrator\Desktop\toy-app\wwwroot /grant Users:rx /t
```

### Create Application Pool
An application pool is essentially a group of processes from which worker threads are allocated.

A site can be configured to create its worker threads within a particular pool.

```powershell
New-WebAppPool -Name "local-website-pool"
```

Results in addition of a lines in the "applicationPools" section of "applicationHost.config"
```xml
        <applicationPools>
            <add name="DefaultAppPool" />
+           <add name="local-website-pool" />
            ...
        </applicationPools>
```

### Add site
Next, create a "site", pointing at our empty directory, and bind it to a port:

```powershell
New-Website -Name "local-website" -ApplicationPool local-website-pool -PhysicalPath c:\Users\Administrator\Desktop\toy-app\wwwroot -Port 8080
```

The above command results in addition of these lines in "applicationHost.config"
```xml
            <site name="local-website" id="2" serverAutoStart="true">
                <application path="/" applicationPool="local-website-pool">
                    <virtualDirectory path="/" physicalPath="c:\Users\Administrator\Desktop\toy-app\wwwroot" />
                </application>
                <bindings>
                    <binding protocol="http" bindingInformation="*:8080:" />
                </bindings>
            </site>
```

### Add application
```powershell
New-WebApplication -name "WebApplication1" -Site 'local-website' -PhysicalPath c:\Users\Administrator\Desktop\toy-app\WebApplication1
```

Results in addition of these lines in "applicationHost.config", after the "wwwroot" <application> section 
```xml
                <application path="/WebApplication1">
                    <virtualDirectory path="/" physicalPath="c:\Users\Administrator\Desktop\toy-app\WebApplication1" />
                </application>
```

### Remove:

The following three powershell commands "undo" the ones above (in reverse order of creation):

```powershell
Remove-WebApplication -name "WebApplication1" -Site 'local-website'
Remove-Website -Name "local-website"
Remove-WebAppPool -Name "local-website-pool"
```

## Configuring via ansible

The corresponding tasks for creating the app pool, site and application  are:

- [win_iss_webapppool](http://docs.ansible.com/ansible/win_iis_webapppool_module.html)
- [win_iss_website](http://docs.ansible.com/ansible/win_iis_website_module.html)
- [win_iss_webapplication](http://docs.ansible.com/ansible/win_iis_webapplication_module.html)

