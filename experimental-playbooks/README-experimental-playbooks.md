# Experimental Playbooks
Example of how to run:
```
cd experimental-playbooks

ansible-playbook -i POC/staging-mark experimental-playbooks/ping_servers.yml --ask-vault-pass

```

## ping_server
Just does a win_ping on the server

Uses:
- http://docs.ansible.com/ansible/win_ping_module.html

## create_file_basic
Creates a single file on the windows server, as a simple copy of a local file

Uses:
- http://docs.ansible.com/ansible/win_copy_module.html

## create_file_from_template
Copy a file based on a template, with variable substitution {{ Foo }}

Uses:
- http://docs.ansible.com/ansible/win_template_module.html

## create_directory
Create a directory

Uses:
- http://docs.ansible.com/ansible/win_file_module.html

Also illustrates:
- use of a variable in playbook

## services
stop_service / start_service playbooks illustrate stopping/starting windows services

Uses:
- http://docs.ansible.com/ansible/win_service_module.html

## install_windows_feature
install_iis - illustrates installation of IIS on a windows server

This playbook enables the IIS webserver. It takes about 2 minutes to run
(or almost no time at all, if the server is already enabled)

Uses:
- http://docs.ansible.com/ansible/win_feature_module.html


## firewall
add_firewall_rules - illustrates how to open a port in the firewall

## win_shell
win_shell_example - illustrates use of the "win_shell" module.

This only works with ansible version >= 2.2.

Uses:
-https://docs.ansible.com/ansible/win_shell_module.html
